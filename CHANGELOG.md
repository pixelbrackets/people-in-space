# Changelog

2020-04-23 Dan Untenzu <mail@pixelbrackets.de>

  * 1.1.0
  * FEATURE Add CI

2020-04-23 Dan Untenzu <mail@pixelbrackets.de>

  * 1.0.0
  * FEATURE Inital version
