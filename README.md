# People in Space

[![Version](https://img.shields.io/packagist/v/pixelbrackets/people-in-space.svg?style=flat-square)](https://packagist.org/packages/pixelbrackets/people-in-space/)
[![Build Status](https://img.shields.io/gitlab/pipeline/pixelbrackets/people-in-space?style=flat-square)](https://gitlab.com/pixelbrackets/people-in-space/pipelines)
[![Made With](https://img.shields.io/badge/made_with-php-blue?style=flat-square)](https://gitlab.com/pixelbrackets/people-in-space#requirements)
[![License](https://img.shields.io/badge/license-gpl--2.0--or--later-blue.svg?style=flat-square)](https://spdx.org/licenses/GPL-2.0-or-later.html)

This package returns how many humans are in space right now.

![Demo](./docs/screen.png)

## Requirements

- PHP

## Installation

Packagist Entry https://packagist.org/packages/pixelbrackets/people-in-space/

## Source

https://gitlab.com/pixelbrackets/people-in-space

## License

GNU General Public License version 2 or later

The GNU General Public License can be found at http://www.gnu.org/copyleft/gpl.html.

## Author

Dan Untenzu (<mail@pixelbrackets.de> / [@pixelbrackets](https://pixelbrackets.de))

## Changelog

See [./CHANGELOG.md](CHANGELOG.md)

## Contribution

This script is Open Source, so please use, patch, extend or fork it.
