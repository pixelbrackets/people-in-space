<?php

namespace Pixelbrackets\HumansInSpace;

use GuzzleHttp\Client;

class HumansInSpace
{
    /**
     * The number of humans in space
     *
     * @var integer
     */
    protected $number = 0;

    /**
     * Humans in Space
     *
     * @return string
     */
    public function __construct()
    {
        // Get latest data from open notify API
        $client = new Client();
        $response = $client->request(
            'GET',
            'http://api.open-notify.org/astros.json'
        );
        $humansInSpace = json_decode((string) $response->getBody());
        $this->number = $humansInSpace->number;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }
}
