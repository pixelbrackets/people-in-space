<?php

require __DIR__ . '/../vendor/autoload.php';

$responseBody = [
    'number' => (new \Pixelbrackets\HumansInSpace\HumansInSpace())->getNumber(),
];

echo json_encode($responseBody, JSON_PRETTY_PRINT);
